package com.atlantis;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "metrics")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "Metrics.findAll", query = "SELECT m FROM Metrics m"),
	@NamedQuery(name = "Metrics.findById", query = "SELECT m FROM Metrics m WHERE m.id = :id"),
	@NamedQuery(name = "Metrics.findByDeviceid", query = "SELECT m FROM Metrics m WHERE m.deviceid = :deviceid"),
	@NamedQuery(name = "Metrics.findByDate", query = "SELECT m FROM Metrics m WHERE m.date = :date"),
	@NamedQuery(name = "Metrics.findByValue", query = "SELECT m FROM Metrics m WHERE m.value = :value"),
	@NamedQuery(name = "Metrics.findByUnit", query = "SELECT m FROM Metrics m WHERE m.unit = :unit")})
public class Metrics implements Serializable, DAO {

	private static final long serialVersionUID = 1L;
	
	@Id
    @Basic(optional = false)
    @Column(name = "id")
	private Integer id;
	
	@Column(name = "deviceid")
	private Integer deviceid;
	
	@Basic(optional = false)
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@Column(name = "value")
	private Integer value;
	
	@Column(name = "unit")
	private String unit;
	
	public Metrics() {
	}

	public Metrics(Integer id) {
		this.id = id;
	}

	public Metrics(Date date, Integer deviceid, Integer id, String unit, Integer value) {
		this.date = date;
		this.deviceid = deviceid;
		this.id = id;
		this.unit = unit;
		this.value = value;
	}
	
	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public Integer getDeviceid() {
		return deviceid;
	}

	@Override
	public void setDeviceid(Integer deviceid) {
		this.deviceid = deviceid;
	}

	@Override
	public Date getDate() {
		return date;
	}

	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public Integer getValue() {
		return value;
	}

	@Override
	public void setValue(Integer value) {
		this.value = value;
	}

	@Override
	public String getUnit() {
		return unit;
	}

	@Override
	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Metrics)) {
			return false;
		}
		Metrics other = (Metrics) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Metrics[ '" + date + "' " + deviceid + " " + id + " '" + unit + "' " + value + " ]";
	}
	
	@Override
	public void toData() {
		Connection c;
		PreparedStatement p;
		
		try {
			c = DriverManager.getConnection("jdbc:mysql://localhost:3306/calculatedmetrics?zeroDateTimeBehavior=convertToNull&useSSL=false", "root", "dbpwd");
			p = c.prepareStatement("INSERT INTO metrics (id, deviceid, date, value, unit) " + "VALUES (?, ?, ?, ?, ?)");
			
			p.setInt(1, this.getId());
			p.setInt(2, this.getDeviceid());
			p.setDate(3, new java.sql.Date(this.getDate().getTime()));
			p.setInt(4, this.getValue());
			p.setString(5, this.getUnit());
			
			p.executeUpdate();
			
			System.out.println("INSERT INTO metrics (id, deviceid, date, value, unit) " + "VALUES (" +
					this.getId() + ", " + this.getDeviceid() + ", '" + new java.sql.Date(this.getDate().getTime()) +
					"', " + this.getValue() + ", '" + this.getUnit() + "');");
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
}