package com.atlantis.jsf;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;
import javax.xml.bind.DatatypeConverter;

@Named(value = "jSFManagedBean")
@SessionScoped
public class JSFManagedBean implements Serializable {

	@Resource(mappedName = "jms/dest")
	private Queue dest;

	@Inject
	@JMSConnectionFactory("java:comp/DefaultJMSConnectionFactory")
	private JMSContext context;
	
	private String login;
	private String password;
	
	public JSFManagedBean() {
	}

	private void sendJMSMessageToDest(String messageData) {
		context.createProducer().send(dest, messageData);
	}
	
	public void send() {
		this.sendJMSMessageToDest(this.login);
	}
	
	public void auth() {
		if(this.login.equals("admin") && this.password.equals("03AC674216F3E15C761EE1A5E255F067953623C8B388B4459E13F978D7C846F4")) {
			System.out.println("good");
			FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "index.jsp");
		} else {
			System.out.println("bad");
		}
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
		password = DatatypeConverter.printHexBinary(hash);
		this.password = password;
	}
}