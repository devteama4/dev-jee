package com.atlantis.jsf;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@MessageDriven(activationConfig = {
	@ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/dest"),
	@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class MessageDriverBean implements MessageListener {
	
	public MessageDriverBean() {
	}
	
	@Override
	public void onMessage(Message message) {
		try {
			TextMessage tmsg = (TextMessage)message;
			if(tmsg != null)
				System.out.println(tmsg.getText());
		} catch (JMSException ex) {
			Logger.getLogger(MessageDriverBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}