package com.atlantis.service;

import com.atlantis.Metrics;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

@Stateless
@Path("metrics")
public class MetricsFacadeREST extends AbstractFacade<Metrics> {

	@PersistenceContext(unitName = "AtlantisPU")
	private EntityManager em;

	public MetricsFacadeREST() {
		super(Metrics.class);
	}

	@POST
	@Path("/post")
    @Override
    @Consumes(MediaType.APPLICATION_JSON)
	public void create(Metrics entity) {
		System.out.println("create " + entity.toString());
		//super.create(entity);
	}
	
	@PUT
	@Path("/put")
    @Consumes(MediaType.APPLICATION_JSON)
	public void toDB(Metrics m) {
		System.out.println("toDB " + m.toString());
		m.toData();
	}

	@PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
	public void edit(@PathParam("id") Integer id, Metrics entity) {
		System.out.println("edit " + entity.toString());
		super.edit(entity);
	}

	@DELETE
    @Path("{id}")
	public void remove(@PathParam("id") Integer id) {
		System.out.println("remove " + id.toString());
		super.remove(super.find(id));
	}

	@GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
	public Metrics find(@PathParam("id") Integer id) {
		System.out.println("find " + id.toString());
		return super.find(id);
	}
	
	@GET
    @Path("/verify/{id}")
    @Produces(MediaType.APPLICATION_JSON)
	public boolean verify(@PathParam("id") String str) {
		System.out.println("verify " + str);
		
		return true;
	}
	
	@GET
	@Path("/get")
	public void getToken(@Context HttpHeaders headers) {
		String str = headers.getHeaderString("Authorization");
		System.out.println("token " + str);
	}
	
	@GET
    @Override
	@Produces(MediaType.APPLICATION_JSON)
	public List<Metrics> findAll() {
		System.out.println("findAll");
		return super.findAll();
	}

	@GET
	@Path("/getjson")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Metrics> getJson(@Context HttpHeaders headers) {
		System.out.println("getjson");
		
		String str = headers.getHeaderString("Authorization");
		System.out.println("token " + str);
		return super.findAll();
	}

	@GET
    @Path("{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
	public List<Metrics> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
		System.out.println("findRange " + from.toString());
		return super.findRange(new int[]{from, to});
	}

	@GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
	public String countREST() {
		return String.valueOf(super.count());
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}