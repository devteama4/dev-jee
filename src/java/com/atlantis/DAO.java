package com.atlantis;

import java.util.Date;

public interface DAO {
	Integer getId();
	void setId(Integer id);
	
	Integer getDeviceid();
	void setDeviceid(Integer deviceid);
	
	Date getDate();
	void setDate(Date date);
	
	Integer getValue();
	void setValue(Integer value);
	
	String getUnit();
	void setUnit(String unit);
	
	void toData();
}