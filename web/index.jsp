<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Backoffice</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    </head>
    <body>
		<style>
			table,th,td{border:2px solid black;}
		</style>
        <h1>Admin backoffice</h1>
        <br>

        <div id="DeviceByEmployee">
            <h2>Looking for an employee ? :</h2>
            <br>
            <form action="">
                <label>Id of the employee : </label>
                <input type="idUser" id="idUser" name="idUser" value="" size="10px" />
                <input type="button" id="getUser" value="Submit"/>
            </form>
        </div>

		<div id="table"></div>

		<script>
			$(document).ready(function () {

				$('#getUser').click(function () {
					var getUser = $("#idUser").val();
					if (getUser == '') {
						alert("Enter Some Text In Input Field");
					} else {
						$("#table").empty();

						var url = 'https://0281fc4f.ngrok.io/api/Devices/DeviceByEmployee/';

						$.getJSON(url + getUser, function (result) {
							var i = 0;
							$("#table").append("<br>");
							$("#table").append("<table><tr><th>ID</th><th>Mac Adress</th><th>Name</th><th>Employee ID</th><th>Category ID</th></tr>")
							while (result != null) {
								$("#table").append("<tr>")
								$("#table").append("<td>" + result[i].Id + "</td>");
								$("#table").append("<td>" + result[i].MacAdress + "</td>");
								$("#table").append("<td>" + result[i].Name + "</td>");
								$("#table").append("<td>" + result[i].Employee + "</td>");
								$("#table").append("<td>" + result[i].DeviceCategory + "</td>");
								$("#table").append("</tr>");

								++i;
							}
							$("#table").append("</table>");
						});
					}
				});
			});
		</script>
    </body>
</html>
